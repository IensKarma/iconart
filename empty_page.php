<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:php="http://php.net/xsl">
    <head>
        <link rel="icon" href="/favicon.ico?v2" type="image/x-icon"></link>
        <link rel="shortcut icon" href="/favicon.ico?v2" type="image/x-icon"></link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <meta name="yandex-verification" content="455787739c3c8934"></meta>
        <title>Иконописная мастерская Екатерины Ильинской в Москве, Иконы на заказ</title>
        <meta name="keywords" content="Иконописная мастерская, икона, купить икону, икона на заказ, иконы в наличии, икона в подарок"></meta>
        <meta name="description" content="Предлагаем вам создать уникальное произведение искусства, настоящую семейную реликвию-икону для вашей семьи"></meta>
        <link rel="stylesheet" type="text/css" href="http://static.icon-art.ru/css/style_screen.css"></link>
        <link rel="stylesheet" type="text/css" href="http://static.icon-art.ru/css/style_mob.css" media="all"></link>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,300&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"></link>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"></meta>
        <meta name="viewport" content="width=450px"></meta>
        <meta name="yandex-verification" content="455787739c3c8934"></meta>
        <meta name="google-site-verification" content="eTycel61Ca7aVj4PmI8SdMMAn1KoSK6Ql2_IERNZndA"></meta>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-49635812-1', 'icon-art.ru');
            ga('send', 'pageview');

        </script>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?105"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://static.icon-art.ru/tabs.v.1.2.js"></script>
        <script type="text/javascript">

            $(function () {

                $(window).scroll(function () {

                    if ($(this).scrollTop() != 0) {

                        $('#toTop').fadeIn();

                    } else {

                        $('#toTop').fadeOut();

                    }

                });

                $('#toTop').click(function () {

                    $('body,html').animate({scrollTop: 0}, 800);

                });

            });

        </script>
    </head>
    <body>
        <div class="snap-drawers">
            <div class="snap-drawer snap-drawer-left">
                <div>
                    <strong>Главное меню</strong>
                    <ul>
                        <li><a href="/">Главная</a></li>
                        <li><a href="/Ikony_v_nalichii.html">Иконы в наличии</a></li>
                        <li><a href="/Alfavit.html">Алфавитный указатель</a></li>
                        <li><a href="/calendar/Pokrovitel.html">Узнай своего покровителя</a></li>
                        <li><a href="/feedback/index.html">Контакты</a></li>
                    </ul>
                    <strong class="lower-h3">Категории</strong>
                    <ul>
                        <li><a href="/calendar">Православный календарь</a></li>
                        <li><a href="/Ikona_v_podarok.html">Икона в подарок</a></li>
                        <li><a href="/Ikony_v_nalichii.html">Иконы в наличии</a></li>
                        <li><a href="/Specialnye_predlozhenija.html">Спецпредложения</a></li>
                        <li><a href="/Kak_sdelat_zakaz.html">Как сделать заказ</a></li>
                        <li><a href="/Our_Recent_Work.html">Наши новые работы</a></li>
                        <li><a href="/Mernaja_ikona.html">Мерная икона</a></li>
                        <li><a href="/Semejjnye_ikony.html">Семейные иконы</a></li>
                        <li><a href="/Venchalnye_ikony.html">Венчальные иконы</a></li>
                        <li><a href="/Imennye_ikony.html">Именная икона</a></li>
                        <li><a href="/Ikony_Bogorodicy.html">Иконы Богородицы</a></li>
                        <li><a href="/Ikony_Spasitelja.html">Иконы Спасителя</a></li>
                        <li><a href="/Otzyvy_zakazchikov.html">Отзывы</a></li>
                        <li><a href="/Skladni_iz_karelskojj_berezy.html">Складни</a></li>
                        <li><a href="/Ikony_s_dragocennymi_kamnjami.html">Иконы с драгоценными камнями</a></li>
                        <li><a href="/Chudotvornye_ikony.html">Чудотворные иконы</a></li>
                        <li><a href="/Zhivopisnye_ikony.html">Живописные иконы</a></li>
                        <li><a href="/Restavracija.html">Реставрация</a></li>
                        <li><a href="/Napisanie_ikon_pod_oklad.html">Написание икон под оклад</a></li>
                        <li><a href="/Kioty_i_firmennye_korobki-shkatulki.html">Киоты и шкатулки</a></li>
                        <li><a href="/Domashnijj_ikonostas.html">Домашний иконостас</a></li>
                        <li><a href="/Doski_dlja_ikon.html">Доски для икон</a></li>
                        <li><a href="/Chto_podarit_na_rozhdestvo.html">Подарок на Рождество</a></li>
                        <li><a href="/Chto_podarit_na_Paskhu.html">Что подарить на Пасху</a></li>
                        <li><a href="/O_masterskojj.html">О мастерской</a></li>
                        <li><a href="/Rabota_v_khramakh.html">Работа в храмах</a></li>
                        <li><a href="/Ikona.html">Икона</a></li>
                        <li><a href="/Izdatelstvo.html">Издательская деятельность</a></li>
                        <li><a href="/Obuchenie_ikonopisi.html">Обучение иконописи</a></li>
                        <li><a href="/Video.html">Видео и публикации</a></li>
                        <li><a href="http://forum.icon-art.ru/">Форум</a></li>
                        <li><a href="/feedback/index.html">Контакты</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <a href="#" id="open-left"></a>
        <div class="wrapper snap-content" id="content">
            <a name="top"></a>
            <div class="container">
                <div class="head">
                    <div class="logo"><img src="http://static.icon-art.ru/images/logo.png" alt="Логотип мастерской Екатерины Ильинской"></img></div>
                    <div class="cont">
                        <div class="contct">
                            <p>г. Москва, ул. Кооперативная, д.4,<br></br> корп.9, под.2, цокольный этаж.</p>
                            <span>email:<a href="mailto:info@icon-art.ru"> info@icon-art.ru</a></span>
                            <p><a href="tel:+74956425516">+7 (495) 642-55-16</a>, <a href="tel:+79265341980">+7 (926) 534-19-80</a></p>
                        </div>
                        <div class="search">
                            <form action="/search/" method="get">
                                <input type="text" name="q" value=""></input>
                            </form>
                        </div>
                        <div class="lup">
                            <img src="http://static.icon-art.ru/images/lupa.png"></img>
                        </div>
                        <div class="buttons"></div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="main_block">
                    <div class="hed">
                        <div class="l"></div>
                        <div class="nav">
                            <div class="left_part">
                                <img src="http://static.icon-art.ru/images/left-fig.png"></img>
                            </div>
                            <div class="nav_center">
                                <ul class="menu_main">
                                    <li><a href="/">Главная</a></li> 
                                    <li><a href="/Ikony_v_nalichii.html">Иконы в наличии</a></li> 
                                    <li><a href="/Alfavit.html">Алфавитный указатель</a></li> 
                                    <li><a href="/calendar/Pokrovitel.html">Узнай своего покровителя</a></li>                                   
                                    <li class="ul-wr"><a href="#">Иконы</a><ul class="deep"><li><a href="/Our_Recent_Work.html">Наши новые работы</a></li><li><a href="/Mernaja_ikona.html">Мерные иконы</a></li><li><a href="/Semejjnye_ikony.html">Семейные иконы</a></li><li><a href="/Venchalnye_ikony.html">Венчальные иконы</a></li><li><a href="/Imennye_ikony.html">Именные иконы</a></li><li><a href="/Zhivopisnye_ikony.html">Живописные иконы</a></li><li><a href="/Ikony_Spasitelja.html">Иконы Спасителя</a></li><li><a href="/Ikony_Bogorodicy.html">Иконы Богородицы</a></li><li><a href="/Ikony_s_dragocennymi_kamnjami.html">Иконы с драгоценными камнями</a></li><li><a href="/Napisanie_ikon_pod_oklad.html">Написание икон под оклад</a></li><li><a href="/Skladni_iz_karelskojj_berezy.html">Складни</a></li><li><a href="/Chudotvornye_ikony.html">О чудотворных иконах</a></li></ul></li>    
                                    <li><a href="/feedback/index.html">Контакты</a></li>  
                                </ul>
                            </div>
                            <div class="right_part">
                                <img src="http://static.icon-art.ru/images/right-fig.png"></img>
                            </div>
                        </div>
                        <div class="r"></div>
                    </div>
                    <div class="clear"></div>
                    <div class="cont_wrapp">
                        <div class="left_side">
                            <p> </p>
                            <ul class="sidebar">
                                <li><a href="/calendar">Православный календарь</a></li>
                                <li><a href="/Ikona_v_podarok.html">Икона в подарок</a></li>
                                <li><a href="/Ikony_v_nalichii.html" style="color:#FF4D00">Иконы в наличии</a></li>
                                <li><a href="/Chto_podarit_na_rozhdestvo.html">Подарок на Рождество</a></li>
                                <li><a href="/Specialnye_predlozhenija.html">Спецпредложения</a></li>
                                <li><a href="/Kak_sdelat_zakaz.html">Как сделать заказ</a></li>
                                <li><a href="/Otzyvy_zakazchikov.html">Отзывы</a></li>
                                <li><a href="/Restavracija.html">Реставрация</a></li>
                                <li><a href="/Kioty_i_firmennye_korobki-shkatulki.html">Киоты и шкатулки</a></li>
                                <li><a href="/Domashnijj_ikonostas.html">Домашний иконостас</a></li>
                                <li><a href="/Doski_dlja_ikon.html">Доски для икон</a></li>
                                <li><a href="/Chto_podarit_na_Paskhu.html">Что подарить на Пасху</a></li>
                                <li><a href="/O_masterskojj.html">О мастерской</a></li>
                                <li><a href="/Rabota_v_khramakh.html">Работа в храмах</a></li>
                                <li><a href="/Ikona.html">Икона</a></li>
                                <li><a href="/Izdatelstvo.html">Издательская деятельность</a></li>
                                <li><a href="/Obuchenie_ikonopisi.html">Обучение иконописи</a></li>
                                <li><a href="/Video.html">Видео и публикации</a></li>
                                <li><a href="http://forum.icon-art.ru/">Форум</a></li>
                                <li><a href="/feedback/index.html">Контакты</a></li>
                            </ul>
                            <div class="screen">
                                <div class="soc-screen">
                                    <a href="/mailing/" title="Подписка Icon-Art.Ru" id="mailing">Рассылк@</a>
                                </div>
                                <div id="social-buttons">
                                    <a href="https://www.facebook.com/PravoslavEveryDay" target="_blank">
                                        <img src="http://static.icon-art.ru/images/fb.png"></img>
                                    </a>
                                    <a href="http://vk.com/club57219595" target="_blank">
                                        <img src="http://static.icon-art.ru/images/vk.png"></img>
                                    </a>
                                    <div class="clear"></div>
                                    <a class="instagram" target="blank" href="http://instagram.com/ikonopisnayamasterskaei">
                                        <img src="http://static.icon-art.ru/images/inst.png"></img>
                                    </a>
                                    <a class="youtube" target="blank" href="http://www.youtube.com/channel/UCb3L75mMMguWG6LTX-zV0nA/videos">
                                        <img src="http://static.icon-art.ru/images/yout.png"></img>
                                    </a>
                                    <a class="goog" target="blank" href="https://plus.google.com/114100181042973133851/photos">
                                        <img src="http://static.icon-art.ru/images/gplus.png"></img>
                                    </a>
                                </div>
                                <div class="soc_seti">
                                    <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                                    <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="icon" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir"></div>
                                    <div class="like">
                                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
                                        <script type="text/javascript">VK.init({apiId: 4742436, onlyWidgets: true});</script>
                                        <div id="vk_like"></div>
                                        <script type="text/javascript">VK.Widgets.Like("vk_like", {type: "button"});</script>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="main">
                            <div class="main_wrapper">
                                <!--CONTENT HERE-->
                            </div>
                        </div>
                    </div>
                    <a id="toTop">
                        <p>Наверх</p>
                    </a>
                    <div class="clear"></div>
                    <div class="mobile">
                        <div>
                            <a href="/mailing/" title="Подписка Icon-Art.Ru" id="mailing">Рассылк@</a>
                        </div>
                        <div id="social-buttons">
                            <a href="https://www.facebook.com/PravoslavEveryDay" target="_blank">
                                <img src="http://static.icon-art.ru/images/fb.png">
                            </a>
                            <a href="http://vk.com/club57219595" target="_blank">
                                <img src="http://static.icon-art.ru/images/vk.png">
                            </a>
                            <div class="clear"></div>
                            <a class="instagram" target="blank" href="http://instagram.com/ikonopisnayamasterskaei">
                                <img src="http://static.icon-art.ru/images/inst.png">
                            </a>
                            <a class="youtube" target="blank" href="http://www.youtube.com/channel/UCb3L75mMMguWG6LTX-zV0nA/videos">
                                <img src="http://static.icon-art.ru/images/yout.png">
                            </a>
                            <a class="goog" target="blank" href="https://plus.google.com/114100181042973133851/photos">
                                <img src="http://static.icon-art.ru/images/gplus.png">
                            </a>
                        </div>
                        <div class="soc_seti">
                            <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                            <div class="yashare-auto-init" data-yasharel10n="ru" data-yasharetype="icon" data-yasharequickservices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir"></div>
                            <div class="like">
                                <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
                                <script type="text/javascript">VK.init({apiId: 4742436, onlyWidgets: true});</script>
                                <div id="vk_like"></div>
                                <script type="text/javascript">VK.Widgets.Like("vk_like", {type: "button"});</script>
                                <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Ficon-art.ru&width&layout=button_count&action=like&show_faces=true&share=false&height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowtransparency="true"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="foot">
                        <div class="map">
                            <div class="map_innner">
                                <a href="/feedback/index.html">
                                    <img src="http://static.icon-art.ru/images/map.jpg" width="308" height="162">
                                </a>
                            </div>
                        </div>
                        <div class="contacts">
                            <div class="contacts_head">
                                <p>© 2015 «Иконописная мастерская Екатерины Ильинской».<br /><br />Все права на данной странице защищены</p>
                            </div>
                            <div class="con_left">
                                <div class="con_left_inner">
                                    <p>Звоните:</p>
                                    <span>
                                        <a href="tel:+74956425516">+7 (495) 642-55-16</a>
                                        <br />
                                        <br />
                                        <a href="tel:+74955341980">+7 (926) 534-19-80</a>
                                    </span>
                                    <div class="left_bottom">
                                        <p>г. Москва, м. Спортивная, ул. Кооперативная, д.4,  корп.9, подъезд 2,  цокольный этаж.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="con_right">
                                <div class="con_right_inner">
                                    <p>
                                        Пн-Пт: с 9:00 до 20:00 Сб: с 12:00 до 17:00 Вc - Выходной.
                                        <br />
                                        <br />
                                        Так же по предварительной договорённости мы примем Вас в любое удобное для вас время.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="other">
                            <div class="visa_mcard">
                                <div class="visa">
                                    <img src="http://static.icon-art.ru/images/visa.png">
                                </div>
                                <div class="mcard">
                                    <img src="http://static.icon-art.ru/images/mcard.png">
                                </div>
                            </div>
                            <div class="fbvk"></div>
                            <div class="counter">
                                <div id="counters">
                                    <script type="text/javascript">
                                        (function (d, w, c) {
                                            (w[c] = w[c] || []).push(function () {
                                                try {
                                                    w.yaCounter19021459 = new Ya.Metrika({id: 19021459,
                                                        webvisor: true,
                                                        clickmap: true,
                                                        trackLinks: true,
                                                        accurateTrackBounce: true});
                                                } catch (e) {
                                                }
                                            });

                                            var n = d.getElementsByTagName("script")[0],
                                                    s = d.createElement("script"),
                                                    f = function () {
                                                        n.parentNode.insertBefore(s, n);
                                                    };
                                            s.type = "text/javascript";
                                            s.async = true;
                                            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                                            if (w.opera == "[object Opera]") {
                                                d.addEventListener("DOMContentLoaded", f, false);
                                            } else {
                                                f();
                                            }
                                        })(document, window, "yandex_metrika_callbacks");
                                    </script>
                                    <noscript><div><img src="//mc.yandex.ru/watch/19021459" style="position:absolute; left:-9999px;" alt=""></img></div></noscript>
                                    <script type="text/javascript">document.write("<a href='http://www.liveinternet.ru/click' " + "target=_blank><img src='http://counter.yadro.ru/hit?t53.6;r" + escape(document.referrer) + ((typeof (screen) == "undefined") ? "" : ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ? screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) + ";i" + escape("Жж" + document.title.substring(0, 80)) + ";" + Math.random() + "' alt='' title='LiveInternet: показано число просмотров и посетителей за 24 часа' " + "border=0 width=88 height=31><\/a>")</script><a href="http://www.liveinternet.ru/click" target="_blank"></a><a href="http://top100.rambler.ru/top100/"></a><a href="http://top100.rambler.ru/top100/" target="_blank"></a>
                                </div>
                            </div>
                            <div class="inst"></div>
                        </div>
                    </div>
                    <div class="lb"></div>
                    <div class="top"></div>
                    <div class="left1"></div>
                    <div class="bottom"></div>
                    <div class="right1"></div>
                    <div class="rb"></div>
                </div>
            </div>
        </div>
    </body>
</html>