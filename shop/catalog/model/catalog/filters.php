<?php

/**
 * Description of filters
 *
 * @author Nikita Bulatenko <nikita.bulatenko@gmail.com>
 * @version 20130706
 */
class ModelCatalogFilters extends Model {

    public function getOptionsByCategory($category_id, $language_id) {
        $result = array();
        $query = $this->db->query("
SELECT DISTINCT 
  od.`option_id`,
  od.`name` AS option_name,
  ovd.`option_value_id` AS value_id,
  ovd.`name` AS value_name 
FROM
  " . DB_PREFIX . "product_option_value pov 
  INNER JOIN " . DB_PREFIX . "option_description od 
    ON od.`option_id` = pov.`option_id` 
  INNER JOIN `" . DB_PREFIX . "product_to_category` pc 
    ON pc.`product_id` = pov.`product_id` 
  INNER JOIN " . DB_PREFIX . "option_value ov 
    ON ov.`option_value_id` = pov.`option_value_id` 
  INNER JOIN " . DB_PREFIX . "option_value_description ovd 
    ON ovd.`option_value_id` = pov.`option_value_id` 
WHERE od.`language_id` = " . $language_id . " AND od.option_id != 14  
  AND ovd.`language_id` = " . $language_id . " 
  AND pc.`category_id` = " . $category_id . " ORDER BY od.name, ov.sort_order, ovd.name");
        foreach ($query->rows as $row) {
            if (empty($result[$row['option_name']])) {
                $result[$row['option_name']] = array('name' => $row['option_name'], 'values' => array());
            }
            $result[$row['option_name']]['values'][$row['value_name']] = $row['value_name'];
        }
        $attributes = $this->getAttributesByCategory($category_id, $language_id);
        foreach ($attributes as $attr => $values) {
            foreach ($values as $value) {
                $value = trim($value);
                if (empty($result[$attr])) {
                    $result[$attr] = array('name' => $attr, 'values' => array());
                }
                $result[$attr]['values'][$value] = $value;
            }
        }
        if(!empty($result['Количество диодов'])) {
            sort($result['Количество диодов']['values'], SORT_NUMERIC);
        }

        return $result;
    }

    public function getAttributesByCategory($category_id, $language_id) {
        $result = array();
        $query = $this->db->query("
SELECT DISTINCT 
ad.`name` AS option_name, pa.`text` AS value_name
FROM
  " . DB_PREFIX . "attribute_description ad 
  INNER JOIN " . DB_PREFIX . "product_attribute pa ON pa.`attribute_id` = ad.`attribute_id`
  INNER JOIN " . DB_PREFIX . "product_to_category pc ON pc.`product_id` = pa.`product_id`
  
WHERE ad.`language_id` =  " . $language_id . " 
  AND pc.`category_id` = " . $category_id . " ORDER BY ad.name, pa.text");

        foreach ($query->rows as $row) {
            $result[$row['option_name']][] = $row['value_name'];
        }
        return $result;
    }

    public function getManufacturersByCategory() {
        $result = array();
        $query = $this->db->query("SELECT DISTINCT m.* FROM " . DB_PREFIX . "manufacturer m
INNER JOIN " . DB_PREFIX . "product p ON p.`manufacturer_id` = m.`manufacturer_id`
INNER JOIN " . DB_PREFIX . "product_to_category pc ON pc.`product_id` = p.`product_id`
WHERE pc.`category_id` = '" . $this->category_id . "'");
        foreach ($query->rows as $row) {
            $result[$row['name']] = $row['manufacturer_id'];
            //$result[$row['manufacturer_id']] = $row['name'];
        }
        ksort($result);
        return $result;
    }

    public function filterProducts($options, $noLimits = false) {
        
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }
        $cache = md5(http_build_query($options));
        //$product_data = (array) $this->cache->get('filter.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . (int) $customer_group_id . '.' . $cache);
        $product_data = array();

        if (!$product_data) {
            $this->load->model('catalog/product');
            $where = $join = '';
            if (!empty($options['category_id'])) {
                $join .= " INNER JOIN " . DB_PREFIX . "product_to_category pc ON pc.`product_id` = p.`product_id` ";
                $where .= ' AND pc.category_id = ' . (int) $options['category_id'];
            }

            if (!empty($options['min_price'])) {
                $where .= ' AND p.price >= ' . (float) $options['min_price'];
            }
            if (!empty($options['max_price'])) {
                $where .= ' AND p.price <= ' . (float) $options['max_price'];
            }

            if (!empty($options['filter_stock'])) {
                $where .= ' AND (p.quantity > 0 OR p.stock_status_id = 7) ';
            }
            if (!empty($options['manufacturers'])) {
                foreach ($options['manufacturers'] as $i => $man) {
                    $options['manufacturers'][$i] = (int) $man;
                }
                $where .= " AND p.manufacturer_id IN ('" . implode("', '", $options['manufacturers']) . "')";
            }
            if (!empty($options['options'])) {
                $escaped = array();
                foreach ($options['options'] as $i => $values) {
                    if (!isset($escaped[$this->db->escape($i)])) {
                        $escaped[$this->db->escape($i)] = array();
                    }
                    foreach ($values as $value) {
                        $escaped[$this->db->escape($i)][] = $this->db->escape($value);
                    }
                }

                $join .= " LEFT JOIN " . DB_PREFIX . "product_attribute pa ON pa.`product_id` = p.`product_id`";
                $join .= " LEFT JOIN " . DB_PREFIX . "attribute_description ad ON ad.`attribute_id` = pa.`attribute_id` AND ad.language_id = " . (int) $this->config->get('config_language_id');
                $join .= " LEFT JOIN " . DB_PREFIX . "product_option_value pov ON pov.`product_id` = p.`product_id`";
                $join .= " LEFT JOIN " . DB_PREFIX . "option_description od ON od.`option_id` = pov.`option_id` AND od.language_id = " . (int) $this->config->get('config_language_id');
                $join .= " LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON ovd.`option_value_id` = pov.`option_value_id` AND ovd.language_id = " . (int) $this->config->get('config_language_id');
                $where .= " AND (";
                foreach ($escaped as $optName => $optValues) {
                    $where .= " ((od.name = '" . $optName . "' AND ovd.name IN ('" . implode("', '", $optValues) . "')) OR (ad.name ='" . $optName . "' AND pa.text IN ('" . implode("', '", $optValues) . "'))) OR ";
                }
                $where = mb_substr($where, 0, -3) . ' )';
            }




            $sql = "SELECT DISTINCT p.product_id FROM " . DB_PREFIX . "product p"
                    . " INNER JOIN " . DB_PREFIX . "product_description pd ON pd.product_id = p.product_id AND pd.language_id = " . (int) $this->config->get('config_language_id')
                    . " LEFT JOIN (SELECT AVG(rating) as rating, product_id FROM " . DB_PREFIX . "review GROUP BY product_id) r ON r.product_id = p.product_id "
                    . $join . " WHERE p.status = '1' " . $where;
            $sort_data = array(
                'pd.name',
                'p.model',
                'p.quantity',
                'p.price',
                'rating',
                'p.sort_order',
                'p.date_added'
            );

            if (isset($options['sort']) && in_array($options['sort'], $sort_data)) {
                if ($options['sort'] == 'pd.name' || $options['sort'] == 'p.model') {
                    $sql .= " ORDER BY LCASE(" . $options['sort'] . ")";
                } else {
                    $sql .= " ORDER BY " . $options['sort'];
                }
            } else {
                $sql .= " ORDER BY p.sort_order";
            }

            if (isset($options['order']) && ($options['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }
            if (!$noLimits) {
                if (isset($options['limit'])) {
                    if ($options['limit'] < 1 || $options['limit'] > 100) {
                        $options['limit'] = 9;
                    } else {
                        $options['limit'] = (int) $options['limit'];
                    }
                } else {
                    $options['limit'] = 9;
                }
                if (isset($options['page'])) {
                    $options['page'] = intval($options['page']);
                    $start = --$options['page'] * $options['limit'];
                }
                $sql .= " LIMIT " . $start . " ," . (int) $options['limit'];
            }
            
            $query = $this->db->query($sql);

            foreach ($query->rows as $result) {
                $product_data[$result['product_id']] = $this->model_catalog_product->getProduct($result['product_id']);
            }
            $this->cache->set('filter.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . (int) $customer_group_id . '.' . $cache, $product_data);
        }
        return $product_data;
    }

    function getTotal($options) {
        $options['limit'] = 99999999;
        return count($this->filterProducts($options, true));
    }

}
