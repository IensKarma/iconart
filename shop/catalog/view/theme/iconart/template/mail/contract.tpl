<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title><?php echo $title; ?></title>
        <style>
            body { font-family: sans-serif; line-height: 1.5;}
            div.container {
                width: 700px;
            }
            table {
                border-collapse: collapse;
                margin-bottom: 20px;
                width: 100%;
            }
            h1 {
                text-align: center;
                width:100%;
            }
            .text-right {
                text-align: right;
            }
            table td {
                border: 1px solid #000;
                padding: 5px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <center><h1>Шаблон договора.</h1></center>
            <p>Переменные</p>
            $base = <?php echo $base; ?> <br />
            $order['order_id'] = <?php echo $order['order_id']; ?> <br />
            $order['email'] = <?php echo $order['email']; ?> <br />
            $order['telephone'] = <?php echo $order['telephone']; ?> <br />
            $order['shipping_address'] = <?php echo $order['shipping_address']; ?> <br />
            $order['firstname'] = <?php echo $order['firstname']; ?> <br />
            $order['lastname'] = <?php echo $order['lastname']; ?> <br />
            <br />
            <br />
            <br />
            <p>$order = </p>
            <pre>
                <?php var_dump($order); ?>
            </pre>
        </div>
    </body>
</html>