<button class="btn btn-default" data-toggle="collapse" data-target="#filtersAccordion"><?php echo $heading_title; ?></button>
<div class="row collapse" id="filtersAccordion">
    <form id='filtersForm'>
        <input type='hidden' name='category_id' value='<?php echo $category_id; ?>' />
        <input type='hidden' name='path' value='<?php echo $path; ?>' />
        <input type='hidden' id="filter_sort" name='sort' value='<?php echo $sort; ?>' />
        <input type='hidden' id="filter_order" name='order' value='<?php echo $order; ?>' />
        <input type='hidden' id="filter_limit" name='limit' value='<?php echo $limit; ?>' />
        <input type='hidden' id="filter_page" name='page' value='<?php echo $page; ?>' />
        <div class="col-sm-4">
            <br />
            <label><input type="checkbox" name="filter_stock" value="1" checked="checked" /> <?php echo $stock_label; ?></label>
        </div>
        <div class="col-sm-4">
            <h4><?php echo $price_label; ?>:</h4>
            <?php if($min_price !== $max_price) { ?>
            <div class="noUiSlider"></div>
            <input type='hidden' id='min_price' name='min_price' value='<?php echo $min_price; ?>' />
            <input type='hidden' id='max_price' name='max_price' value='<?php echo $max_price; ?>' />
            <span class="selectedMinPrice"><?php echo $min_price; ?></span>
            <span class="selectedMaxPrice" style="float:right;"><?php echo $max_price; ?></span>
            <?php } else { ?>
            <span><?php echo $min_price; ?></span>
            <?php } ?>
        </div>
        <div class="col-sm-4">
            <?php if(!empty($manufacturers)) { ?>
            <div class="filters-manufacturers">
                <div class="option-label"><a href="javascript:void(0);"><?php echo $manufacturers_label?></a></div>
                <div class="option-values" style="display:none;">
                    <?php foreach($manufacturers as $manufacturer => $manufacturer_id) { ?>
                    <div class="filters-value"><label><input type="checkbox" name="manufacturers[]" value="<?php echo $manufacturer_id; ?>"> <?php echo $manufacturer; ?></label></div>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
            <div class="filters-options">
                <?php foreach($options as $option_id => $option) { ?>
                <div class="option-label col-sm-12"><?php echo $option['name']?></div>
                <div class="option-values">
                    <?php foreach($option['values'] as $value_id => $value_name) { ?>
                    <div class="col-sm-6"><label><input type="checkbox" name="options[<?php echo $option_id?>][]" value="<?php echo $value_id; ?>"> <?php echo $value_name; ?></label></div>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-sm-12" style="text-align: center;">
            <input class="btn btn-default" type="button" value="<?php echo $show_label; ?>" onclick="resetPage();getProducts();" class="button" />
        </div>
    </form>
</div>
<script>
    $('.noUiSlider').noUiSlider({
        range: [ <?php echo $min_price; ?> , <?php echo $max_price; ?> ],
                start: [ <?php echo $min_price; ?> , <?php echo $max_price; ?> ],
                step: <?php echo $price_step; ?> ,
                slide: function () {
                    var values = $(this).val();
                    $(".selectedMinPrice").text(values[0]);
                    $(".selectedMaxPrice").text(values[1]);
                    $('#min_price').val(values[0]);
                    $('#max_price').val(values[1]);
                }
    });
    function createPager(totalProducts) {
        var loadedPage = parseInt($('#filter_page').val());
        totalProducts = parseInt(totalProducts);
        var limit = parseInt($('#filter_limit').val());
        var productCounter = limit;
        var pageCounter = 1;
        var pagerHtml = '';
        if(loadedPage > 1) {
            pagerHtml += '<li><a href="loadPage(1)">|&lt;</a></li>';
            pagerHtml += '<li><a href="loadPage('+(loadedPage-1)+')">&lt;</a></li>';
        }
        while (productCounter < totalProducts + limit) {
            if (pageCounter == loadedPage) {
                pagerHtml += '<li class="active"><span>' + pageCounter + '</span></li>';
            } else {
                pagerHtml += '<li><a href="javascript:void(0);" onclick="loadPage(' + pageCounter + ')">' + pageCounter + '</a></li>';
            }
            pageCounter++;
            productCounter = pageCounter * limit;
        }
        if(pageCounter !== loadedPage + 1 && totalProducts > 0) {
            pagerHtml += '<li><a href="loadPage('+(loadedPage+1)+')">&gt;</a></li>';
            pagerHtml += '<li><a href="loadPage(' + (pageCounter-1) + ')">&gt;|</a></li>';
        }
        return (pageCounter == 2) ? '' : pagerHtml;
    }
    function createStats(totalProducts) {
        var loadedPage = parseInt($('#filter_page').val());
        var limit = parseInt($('#filter_limit').val());
        totalProducts = parseInt(totalProducts);
        var end = (totalProducts < loadedPage * limit) ? totalProducts : loadedPage * limit;
        var start = --loadedPage * limit + 1;
        var pagesCount = Math.ceil(totalProducts / limit);
        return 'Показано с ' + start + ' по ' + end + ' из ' + totalProducts + ' (всего ' + pagesCount + ' страниц)';
    }

    function loadPage(page) {
        $('#filter_page').val(parseInt(page));
        getProducts();
    }

    function resetPage() {
        $('#filter_page').val(1);
    }

    function getProducts() {
        $('#productsList').html('<div class="row"><img src="  image/ajax-loader.gif" alt="Подождите, ваш запрос обрабатывается" title="Подождите, ваш запрос обрабатывается" /></div>');
        $.post('index.php?route=extension/module/filters/getProductsAjax', $('#filtersForm').serialize(), function (a) {
            var price, colors, productHtml, product = {};
            $('#productsList').html('');
            if (!a.products.length) {
                $('#productsList').append('<h2 class="center">К сожалению по данным критериям ничего не найдено.</h2>');
            } else {
                for (var i in a.products) {
                    product = a.products[i];
                    price = product.price;
                    productHtml = '<div class="product-layout product-list col-xs-12">\
                    <div class="product-thumb">\
                        <div class="image">\
                            <a href="' + product.href + '"><img src="' + product.thumb + '" title="' + product.name + '" alt="' + product.name + '" /></a>\
                        </div>\
                        <div>\
                            <div class="caption">\
                                <h4><a href="' + product.href + '">' + product.name + '</a></h4>\
                                <p>' + product.description + '</p>';
                                if(product.price) {
                                    productHtml += '<p class="price">' + (!product.special ? product.price : '<span class="price-new">' + product.special + '</span> <span class="price-old">' + product.price + '</span>') + '</p>';
                                }
                                if (product.rating) {
                                    productHtml += '<div class="rating">';
                                    for (var i = 1; i <= 5; i++) {
                                        if (product.rating < i) {
                                            productHtml += '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>';
                                        } else {
                                            productHtml += '<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>';
                                        }
                                    }
                                    productHtml += '</div>';
                                }
                            productHtml += '</div>\
                            <div class="button-group">\
                                <button type="button" onclick="cart.addWithPopup(\'' + product.product_id + '\', \'' + product.minimum + '\');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>\
                                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add(\'' + product.product_id + '\');"><i class="fa fa-heart"></i></button>\
                                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add(\'' + product.product_id + '\');"><i class="fa fa-exchange"></i></button>\
                            </div>\
                        </div>\
                    </div>\
                    </div>';
                    $('#productsList').append(productHtml);
                    $('#list-view.active, #grid-view.active').click();
                }
            }
            $('ul.pagination').html(createPager(a.total));
            $('#statistic').html(createStats(a.total));
            var top = $('#content').position().top;
            $(window).scrollTop(top);
        });
        $('#input-limit').attr('onchange', '').unbind('change').bind('change', function () {
            var limit = $(this).val().match(/limit=([0-9]+)/);
            $('#filter_limit').val(limit[1]);
            resetPage();
            getProducts();
        });
        $('#input-sort').attr('onchange', '').unbind('change').bind('change', function () {
            var v = $(this).val();
            var sort = v.match(/sort=([a-z\.]+)/i);
            $('#filter_sort').val(sort[1]);
            var order = v.match(/order=(ASC|DESC)/);
            $('#filter_order').val(order[1]);
            resetPage();
            getProducts();
        });
    }
</script>