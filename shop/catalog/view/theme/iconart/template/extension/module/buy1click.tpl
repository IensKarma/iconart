<div class="buy1click-popup">
    <form class="form-horizontal" method="post" id="buy1clickForm">
        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"
               <fieldset id="buy1clickFields">
            <legend>Купить за 1 клик</legend>
            <div class="form-group required">
                <label for="input-name" class="col-sm-2 control-label">Имя</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="input-name" placeholder="Имя" value="" name="name" />
                </div>
            </div>
            <div class="form-group required">
                <label for="input-phone" class="col-sm-2 control-label">Телефон</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="input-name" placeholder="Телефон" value="" name="phone" />
                </div>
            </div>
            <center>
                <input type="button" class="btn btn-primary" id="b1cSubmit" value="Жду звонка">
            </center>
        </fieldset>
    </form>
    <script type='text/javascript'>//magnific popup requires only 1 root element
        $('#b1cSubmit').click(function () {
            $('div.buy1click-popup div.alert').remove();
            $.ajax('<?php echo $submitUrl; ?>', {
                type: 'POST',
                data: $('#buy1clickForm').serialize(),
                success: function (a) {
                    var alertClass = 'success';
                    if (a.result == 'error') {
                        alertClass = 'danger';

                    } else {
                        $('div.buy1click-popup div.form-group, #b1cSubmit').remove();
                    }
                    $('div.buy1click-popup legend').after('<div class="alert alert-' + alertClass + '">' + a.message + '</div>');
                }
            });
        });
    </script>
</div>

