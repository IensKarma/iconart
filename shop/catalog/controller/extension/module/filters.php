<?php

class ControllerExtensionModuleFilters extends Controller
{

    public function index()
    {
        $this->load->language('extension/module/filters');
        $this->load->model('catalog/filters');
        $data = array(
            'heading_title' => $this->language->get('heading_title'),
            'price_label' => $this->language->get('price_label'),
            'show_label' => $this->language->get('show_label'),
            'stock_label' => $this->language->get('stock_label'),
            'manufacturers_label' => $this->language->get('manufacturers_label'),
        );

        if (isset($this->request->get['path']))
        {
            $path = '';

            $parts = explode('_', (string) $this->request->get['path']);


            $category_id = (int) array_pop($parts);
        } else
        {
            $category_id = 0;
        }

        if (isset($this->request->get['sort']))
        {
            $data['sort'] = $this->request->get['sort'];
        } else
        {
            $data['sort'] = 'p.sort_order';
        }

        if (isset($this->request->get['page']))
        {
            $data['page'] = $this->request->get['page'];
        } else
        {
            $data['page'] = '1';
        }

        if (isset($this->request->get['order']))
        {
            $data['order'] = $this->request->get['order'];
        } else
        {
            $data['order'] = 'ASC';
        }

        if (isset($this->request->get['limit']))
        {
            $data['limit'] = (int) $this->request->get['limit'];
        } else
        {
            $data['limit'] = $this->config->get($this->config->get('config_theme') . '_product_limit');
        }

        $data['path'] = isset($this->request->get['path']) ? $this->request->get['path'] : '';
        $data['category_id'] = $category_id;
        $data['min_price'] = $data['max_price'] = 0;
        if ($category_id > 0)
        {
            $products = $this->model_catalog_product->getProducts(array('filter_category_id' => $category_id));
            foreach ($products as $product)
            {
                $product['price'] = (float) $product['price'];
                if ($data['min_price'] == 0 || ($data['min_price'] > $product['price'] && $product['price'] > 0))
                {
                    $data['min_price'] = $product['price'];
                }
                if ($data['max_price'] == 0 || $product['price'] > $data['max_price'])
                {
                    $data['max_price'] = $product['price'];
                }
            }
            $data['price_step'] = round(($data['max_price'] - $data['min_price']) / 100);
            $data['options'] = $this->model_catalog_filters->getOptionsByCategory($category_id, (int) $this->config->get('config_language_id'));
            $data['manufacturers'] = $this->model_catalog_filters->getManufacturersByCategory();
            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            return $this->load->view('extension/module/filters', $data);
        }
    }

    public function getProductsAjax()
    {
        $this->load->model('catalog/filters');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');
        $results = $this->model_catalog_filters->filterProducts($this->request->post);
        $response = array('products' => array(), 'total' => $this->model_catalog_filters->getTotal($this->request->post));
        foreach ($results as $result)
        {
            if (empty($result))
            {
                continue;
            }
            //if ($result['image']) {
            if ($result['image'])
            {
                $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
            } else
            {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price'))
            {
                $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else
            {
                $price = false;
            }

            if ((float) $result['special'])
            {
                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else
            {
                $special = false;
            }

            if ($this->config->get('config_tax'))
            {
                $tax = $this->currency->format((float) $result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
            } else
            {
                $tax = false;
            }

            if ($this->config->get('config_review_status'))
            {
                $rating = (int) $result['rating'];
            } else
            {
                $rating = false;
            }

            $response['products'][] = array(
                'product_id' => $result['product_id'],
                'thumb' => $image,
                'name' => $result['name'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
                'price' => $price,
                'special' => $special,
                'tax' => $tax,
                'rating' => $result['rating'],
                'reviews' => sprintf($this->language->get('text_reviews'), (int) $result['reviews']),
                'href' => $this->url->link('product/product', 'path=' . $this->request->post['path'] . '&product_id=' . $result['product_id']),
                'in_stock' => ($result['quantity'] > 0 || $result['stock_status'] == 'В наличии'),
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($response));
    }

}
