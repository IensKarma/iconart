<?php

class ControllerExtensionModuleBuy1click extends Controller
{

    public function index()
    {
        $data = array(
            'submitUrl' => $this->url->link('extension/module/buy1click/submit'),
            'product_id' => $this->request->get['product_id']
        );
        $this->response->setOutput($this->load->view('extension/module/buy1click', $data));
    }

    public function submit()
    {
        $json = array(
            'result' => 'success',
            'message' => 'Ваша заявка принята. В ближайшее время мы с Вами свяжемся.'
        );
        $post = $this->request->post;
        if (empty($post['product_id']) || empty($post['name']) || empty($post['phone']))
        {
            $json['result'] = 'error';
            $json['message'] = 'Поля обязательны для заполнения!';
        } else
        {
            $this->load->model('extension/module/buy1click');
            $this->model_extension_module_buy1click->register($post);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
    
    public function submitcart()
    {
        $json = array(
            'result' => 'success',
            'message' => 'Ваша заявка принята. В ближайшее время мы с Вами свяжемся.'
        );
        $post = $this->request->post;
        if (empty($post['name']) || empty($post['phone']))
        {
            $json['result'] = 'error';
            $json['message'] = 'Поля обязательны для заполнения!';
        } else
        {
            $this->load->model('extension/module/buy1click');
            $this->model_extension_module_buy1click->registercart($post);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
    
    public function cart () {
	    $data = array(
            'submitUrl' => $this->url->link('extension/module/buy1click/submitcart'),
            'product_id' => ''
        );
        $this->response->setOutput($this->load->view('extension/module/buy1click', $data));
    }

}
