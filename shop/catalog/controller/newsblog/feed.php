<?php

class ControllerNewsBlogFeed extends Controller
{

    public function index()
    {
        $this->load->language('newsblog/feed');
        $this->load->model('newsblog/article');
        $this->load->model('tool/image');
        $data = array();
        $data['breadcrumbs'] = array(
            array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            ),
            array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('newsblog/feed')
            ),
        );
        $data['heading_title'] = $this->language->get('heading_title');
        $start = 0;
        if (!empty($this->request->get['page']))
        {
            $start = 10 * ($this->request->get['page'] - 1);
        } else {
            $this->request->get['page'] = 1;
        }
        $data['articles'] = $this->model_newsblog_article->getArticles(array(
            'sort' => 'date_added',
            'order' => 'DESC',
            'limit' => 10,
            'start' => $start,
        ));
        $articlesTotal = $this->model_newsblog_article->getTotalArticles(array(
            'sort' => 'date_added',
            'order' => 'DESC',
            'limit' => 999999,
            'start' => 0,
        ));
        foreach ($data['articles'] as &$article)
        {
            $w = $this->config->get($this->config->get('config_theme') . '_image_thumb_width');
            $h = $this->config->get($this->config->get('config_theme') . '_image_thumb_height');
            $article['thumb'] = !empty($article['image']) ? $this->model_tool_image->resize($article['image'], 200, 150) : '';
            $article['href'] = $this->url->link('newsblog/article', 'newsblog_article_id=' . $article['article_id']);
        }
        $pagination = new Pagination();
        $pagination->total = $articlesTotal;
        $pagination->page = $this->request->get['page'];
        $pagination->limit = 10;
        $pagination->url = $this->url->link('newsblog/feed', 'page={page}');

        $data['pagination'] = $pagination->render();
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $this->response->setOutput($this->load->view('newsblog/feed', $data));
    }

}
